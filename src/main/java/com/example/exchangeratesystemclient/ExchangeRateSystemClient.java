package com.example.exchangeratesystemclient;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ExchangeRateSystemClient {

    private final RestTemplate restTemplate;

    public ExchangeRateSystemClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/getAllData")
    public ResponseEntity<HistoricData[]> getDataBeforeLoad() {

        ResponseEntity<HistoricData[]> response = restTemplate.getForEntity("http://exchange-rate-system" +
                "/exchangerates/api/all", HistoricData[].class);
        return response;
    }

    @GetMapping("/loadData")
    public void loadData() {
        restTemplate.getForEntity("http://exchange-rate-system/exchangerates/external",
                Object.class);
    }
}
